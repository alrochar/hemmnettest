//
//  PropertiesViewModelTests.swift
//  PropertiesTestTests
//
//  Created by Alejandro López Rocha on 2022-03-27.
//

import XCTest
@testable import PropertiesTest

final class PropertiesViewModelTest: XCTestCase {
    var viewModel: PropertyListViewModel!
    var mockService: ServiceMock!
    let serviceData = ServiceMockData()

    override func setUp() {
        mockService = ServiceMock(data: serviceData.jsonData)
        viewModel = PropertyListViewModel(propertyService: mockService)
    }

    override func tearDown() {
        mockService = nil
        viewModel = nil
    }

    func test_modelStateAfterFetchData_succeeds() async {
        await viewModel.fetchAllProperties()
        do {
            let properties = try await mockService.fetchAllProperties().items
            XCTAssertEqual(viewModel.state, .success(data: properties))
        } catch {
            XCTFail("Properties could not be created due to: \(error)")
        }
    }


    func test_modelFirstStateLoading_isSucceded() async {
        XCTAssert(viewModel.state == .loading)
    }

    func test_modelStateAfterFetchData_fails() async {
        mockService = ServiceMock(data: serviceData.jsonDataError)
        await viewModel.fetchAllProperties()
        do {
            _ = try await mockService.fetchAllProperties().items
            XCTFail("This test is intended to throw")
        } catch {
            XCTAssertTrue(!error.localizedDescription.isEmpty)
        }
    }
}
