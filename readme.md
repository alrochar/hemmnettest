
# Code test for Hemnet

## What has been done?
1. Build an app with a List of items using SwiftUI and Async/Await
2. Used the MVVM pattern
3. Unit test for service and view model
4. Dependency injection with protocols approach
5. Added image cache

## What could be improved? 
1. Add internet checker
2. Extract views in more structs to be reusable in the project
3. Add favourites
4. Add UI tests

The app has been tested in iPhone 13 Pro in iOS 15.2, but the iOS target is for iOS 14, as described in the PDF file. 

