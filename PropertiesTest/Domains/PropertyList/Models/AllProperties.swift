//
//  AllProperties.swift
//  PropertiesTest
//
//  Created by Alejandro López Rocha on 2022-03-20.
//

import Foundation

struct AllProperties: Codable {
    let items: [Property]
}
