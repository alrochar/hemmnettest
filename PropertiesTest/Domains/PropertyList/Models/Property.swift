//
//  Property.swift
//  PropertiesTest
//
//  Created by Alejandro López Rocha on 2022-03-20.
//

import Foundation

struct Property: Codable, Identifiable, Equatable {
    let id: String
    let type: PropertyType
    let area: String
    let askingPrice: String?
    let averagePrice: String?
    let daysOnHemnet: Int?
    let image: String?
    let livingArea: Float?
    let monthlyFee: String?
    let municipality: String?
    let numberOfRooms: Int?
    let rating: String?
    let streetAddress: String?

    enum PropertyType: String, Codable {
        case area = "Area"
        case highlighted = "HighlightedProperty"
        case property = "Property"
    }
}

struct PropertyItem: Identifiable {
    let type: Property.PropertyType
    let id: String
    let askingPrice: String
    let monthlyFee: String
    let municipality: String
    let area: String
    let daysOnHemnet: String
    let livingArea: String
    let numberOfRooms: String
    let streetAddress: String
    let image: String

    init(property: Property) {
        self.type = property.type
        self.id = property.id
        self.askingPrice = property.askingPrice ?? "Price N/A"
        self.monthlyFee = property.monthlyFee ?? "Monthly fee N/A"
        self.municipality = property.municipality ?? "Municipality N/A"
        self.area = property.area
        self.daysOnHemnet = property.daysOnHemnet?.description ?? "Days N/A"
        self.livingArea = property.livingArea?.description ?? "Area N/A"
        self.numberOfRooms = property.numberOfRooms?.description ?? "Rooms N/A"
        self.streetAddress = property.streetAddress ?? "Street N/A"
        self.image = property.image ?? ""
    }

}

struct AreaItem: Identifiable {
    let id: String
    let area: String
    let rating: String
    let averagePrice: String
    let image: String

    init(property: Property) {
        self.id = property.id
        self.area = property.area
        self.rating = property.rating ?? "Rating N/A"
        self.averagePrice = property.averagePrice ?? "AVG Price N/A"
        self.image = property.image ?? ""
    }
}
