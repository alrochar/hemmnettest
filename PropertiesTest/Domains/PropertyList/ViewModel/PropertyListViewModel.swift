//
//  PropertyListViewModel.swift
//  PropertiesTest
//
//  Created by Alejandro López Rocha on 2022-03-20.
//

import SwiftUI

protocol PropertyListFetchable {
    func fetchAllProperties() async
}


final class PropertyListViewModel: PropertyListFetchable, ObservableObject {
    enum State: Equatable {
        static func == (lhs: State, rhs: State) -> Bool {
            switch(lhs, rhs) {
            case (.loading, .loading):
                return true
            case (.failed, .failed):
                return true
            case (.success(let lhsData), .success(data: let rhsData)):
                return lhsData.count == rhsData.count
            default:
                return false
            }
        }

        case loading
        case success(data: [Property])
        case failed
    }

    @Published private(set) var state: State = .loading

    private let propertyService: PropertyServiceFetchable

    init(propertyService: PropertyServiceFetchable = PropertyService()) {
        self.propertyService = propertyService
    }

    @MainActor func fetchAllProperties() async {
        do {
            let properties = try await propertyService.fetchAllProperties().items
            state = .success(data: properties)
        } catch {
            state = .failed
        }
    }
}
