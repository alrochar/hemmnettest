//
//  PropertiesRequest.swift
//  PropertiesTest
//
//  Created by Alejandro López Rocha on 2022-03-20.
//

import Foundation

enum PropertiesRequest: APIRequest {
    case fetchAllProperties

    var baseURLPath: String {
        APIConstants.baseURL
    }

    var path: String {
        switch self {
        case .fetchAllProperties: return "eXqnGgCY"
        }
    }

    var completeURL: URL? {
        return URL(string: baseURLPath.appending(path))
    }
}

