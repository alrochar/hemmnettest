//
//  PropertyService.swift
//  PropertiesTest
//
//  Created by Alejandro López Rocha on 2022-03-20.
//

import Foundation
import SwiftUI

protocol PropertyServiceFetchable {
    func fetchAllProperties() async throws -> AllProperties
}

class PropertyService: PropertyServiceFetchable {
    func fetchAllProperties() async throws -> AllProperties {
        do {
            guard let url = PropertiesRequest.fetchAllProperties.completeURL else { throw APIError.urlNotFound }
            let (data, _) = try await URLSession.shared.data(from: url)
            return try JSONDecoder().decode(AllProperties.self, from: data)
        } catch {
            throw APIError.genericError
        }
    }
}
