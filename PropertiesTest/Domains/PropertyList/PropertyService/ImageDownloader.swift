//
//  ImageDownloader.swift
//  PropertiesTest
//
//  Created by Alejandro López Rocha on 2022-03-21.
//

import SwiftUI

protocol ImageFetchable {
    func image(from url: URL) async -> Image
}

actor ImageDownloader: ImageFetchable {

    enum ImageError: Error {
        case ImageNotCreated
    }
    private var cache = ImageCache()

    func image(from url: URL) async -> Image {
        if let cached = cache.get(forKey: url.absoluteString) {
            return Image(uiImage: cached)
        }
        guard let image = await downloadImagefrom(url: url.absoluteString) else {
            return Image(systemName: "error")
        }
        cache.set(forKey: url.absoluteString, image: image)
        return Image(uiImage: image)
    }
}

private extension ImageDownloader {
    func downloadImagefrom(url: String) async  -> UIImage? {
        do {
            let url = URL(string: url)!
            let (data, _) = try await URLSession.shared.data(from: url)
            guard let image = UIImage(data: data) else {
                return UIImage(systemName: "error")
            }
            return image
        } catch {
            return UIImage(systemName: "error")
        }
    }
}

private class ImageCache {
    var cache = NSCache<NSString, UIImage>()

    func get(forKey: String) -> UIImage? {
        return cache.object(forKey: NSString(string: forKey))
    }

    func set(forKey: String, image: UIImage) {
        cache.setObject(image, forKey: NSString(string: forKey))
    }
}

struct ImageDownloaderKey: EnvironmentKey {
    static let defaultValue = ImageDownloader()
}

extension EnvironmentValues {
    var imageDownloader: ImageDownloader {
        get { self[ImageDownloaderKey.self] }
        set { self[ImageDownloaderKey.self ] = newValue}
    }
}
