//
//  ContentView.swift
//  PropertiesTest
//
//  Created by Alejandro López Rocha on 2022-03-20.
//

import SwiftUI

struct PropertyListView: View {
    enum Constants {
        static let spacingViews: CGFloat = 15
    }

    @ObservedObject var viewModel = PropertyListViewModel()
    @State var images: [String: Image] = [:]
    var body: some View {
        NavigationView {
            ScrollView(showsIndicators: false) {
                LazyVStack {
                    switch viewModel.state {
                    case .loading:
                        ProgressView("Fetching houses...")
                            .progressViewStyle(CircularProgressViewStyle(tint: .purple))
                    case .success(let properties):
                        ForEach(properties, id: \.id) { property in
                            let propertyItem = PropertyItem(property: property)
                            NavigationLink(destination: PropertyDetailView(property: propertyItem)) {
                                if property.type == .area {
                                    AreaCell(area: AreaItem(property: property)).bodyStyle()
                                } else {
                                    PropertyCell(property: PropertyItem(property: property)).bodyStyle()
                                }
                            }
                        }

                    case .failed:
                        VStack {
                            Text("Ooops something went wrong, try again Something went wrong")
                                .padding()
                            Button("Retry") {
                                retrieveData()
                            }
                            .padding()
                            .background(Color(UIColor.mainGreen))
                            .cornerRadius(10.0)
                            .foregroundColor(.black)
                        }
                    }
                    Divider()
                }
            }.padding()
                .navigationTitle("Properties")
                .navigationBarColor(UIColor.mainGreen, textColor: .white)
        }
        .navigationViewStyle(.stack)
        .onAppear {
            retrieveData()
        }
    }

    func retrieveData() {
        Task {
            await viewModel.fetchAllProperties()
        }
    }
}
