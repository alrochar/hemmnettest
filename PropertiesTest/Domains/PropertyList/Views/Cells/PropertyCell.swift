//
//  PropertyCell.swift
//  PropertiesTest
//
//  Created by Alejandro López Rocha on 2022-03-21.
//

import SwiftUI

struct PropertyCell: View {
    private enum Constants {
        static let imageHeight: CGFloat = 200.0
        static let cornerRadius: CGFloat = 10.0
        static let bottomPadding: CGFloat = 15.0
        static let stackSpacing: CGFloat = 10.0
        static let borderWidth: CGFloat = 2.0
    }
    let property: PropertyItem
    @State var image: Image?
    @Environment(\.imageDownloader) private var imageDownloader

    var body: some View {
            VStack(alignment: .leading, spacing: Constants.stackSpacing) {
                let borderSetup = isHighlighted(property.type) ? (Color.yellow, Constants.borderWidth) : (Color.white, 0)
                if image == nil {
                    ProgressView()
                        .frame(maxWidth: .infinity, minHeight: 200)
                        .cornerRadius(Constants.cornerRadius)
                } else if let image = image {
                    image
                        .resizable()
                        .scaledToFit()
                        .frame(maxWidth: .infinity, minHeight: 200.0)
                        .border(borderSetup.0, width: borderSetup.1)
                        .cornerRadius(Constants.cornerRadius)
                }
                VStack(alignment: .leading, spacing: Constants.stackSpacing) {
                    Text(property.streetAddress).font(.title2)
                    HStack {
                        Text("📍"+property.area).font(.caption)

                    }
                    HStack {
                        Text("🏠"+property.municipality)
                        Spacer()
                        Text("💰"+property.askingPrice)
                        Spacer()
                        Text("📅"+property.daysOnHemnet.description + " days")
                    }
                }
                Spacer()
            }.onAppear {
                Task {
                    if let imageUrl = URL(string: property.image) {
                        image = await imageDownloader.image(from: imageUrl)
                    }
                }
            }
    }

    func isHighlighted(_ propertyType: Property.PropertyType) -> Bool {
        return propertyType == .highlighted
    }
}

struct PropertyCell_Previews: PreviewProvider {
    static var previews: some View {
        PropertyCell(property: PropertyItem(property: Property(id: "123", type: .highlighted, area: "Whatever Area", askingPrice: "123 SEK", averagePrice: nil, daysOnHemnet: 30, image: "https://png.pngtree.com/png-vector/20190223/ourmid/pngtree-vector-house-icon-png-image_695726.jpg", livingArea: 2.0, monthlyFee: "1234 SEK", municipality: "Bromma", numberOfRooms: 2, rating: nil, streetAddress: "hjalmar")))
    }
}
