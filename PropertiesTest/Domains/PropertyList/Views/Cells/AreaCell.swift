//
//  AreaCell.swift
//  PropertiesTest
//
//  Created by Alejandro López Rocha on 2022-03-21.
//

import SwiftUI

struct AreaCell: View {
    // - MARK: Constants
    private enum Constants {
        static let cornerRadius = 10.0
    }
    let area: AreaItem
    @State var image: Image?
    @Environment(\.imageDownloader) private var imageDownloader

    var body: some View {
            VStack(alignment: .leading, spacing: 10.0) {
                Text(area.area).font(.largeTitle).padding(.bottom)
                if image == nil {
                    ProgressView()
                        .frame(maxWidth: .infinity, minHeight: 300.0)
                        .cornerRadius(Constants.cornerRadius)
                } else if let image = image {
                    image.resizable()
                        .frame(height: 300)
                        .cornerRadius(Constants.cornerRadius)
                }
                Text("📍 Stockholm").bold()
                Text("⭐️ Rating: " + area.rating)
                Text("💶 AVG Price: " + area.averagePrice + " kr/m2")
                Spacer()
            }
                .onAppear {
                    Task {
                        if let imageUrl = URL(string: area.image) {
                            image = await imageDownloader.image(from: imageUrl)
                        }
                    }
        }
    }
}
