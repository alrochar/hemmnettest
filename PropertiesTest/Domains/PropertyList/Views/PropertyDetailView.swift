//
//  PropertyDetailView.swift
//  PropertiesTest
//
//  Created by Alejandro López Rocha on 2022-03-21.
//

import SwiftUI

struct PropertyDetailView: View {
    private enum Constants {
        static let imageHeight: CGFloat = 200
        static let roundCorners: CGFloat = 10.0
    }

    @State private var image: Image?
    let property: PropertyItem
    @Environment(\.imageDownloader) private var imageDownloader

    var body: some View {
        VStack {
            if let image = image {
                image
                    .resizable()
                    .frame(maxWidth: .infinity, maxHeight: Constants.imageHeight)
                    .cornerRadius(Constants.roundCorners)
                    .padding(.bottom)

            } else {
                ProgressView("Fetching houses...")
                    .progressViewStyle(CircularProgressViewStyle(tint: .purple))
            }

            HStack {
                VStack(alignment: .leading) {
                    LabelView(text: "NEW").padding(.bottom)
                    Text(property.streetAddress).font(.title2).padding(.bottom, 5)
                    HStack {
                        Text("🏠")
                        Text(property.municipality).font(.footnote)
                    }.padding(.bottom, 5)
                    HStack {
                        Text("💵")
                        Text("House price:").bold()
                        Text(property.askingPrice)

                    }
                    HStack {
                        Text("🛏")
                        Text("Number of rooms:").bold()
                        let rooms = property.numberOfRooms.description
                        Text(rooms)
                    }
                }
                Spacer()
            }
            Spacer()

        }.padding([.leading, .trailing, .top])
            .navigationTitle(property.streetAddress)
            .navigationBarColor(UIColor.mainGreen, textColor: .white)
            .navigationBarTitleDisplayMode(.inline)
            .onAppear {
                Task {
                    if let imageUrl = URL(string: property.image) {
                        image = await imageDownloader.image(from: imageUrl)
                    }
                }

            }
    }

    struct LabelView: View {
        let text: String
        var body: some View {
            Text("#"+text)
                .bold()
                .font(.footnote)
                .padding(.leading, 5)
                .padding(.trailing, 5)
                .background(Color.green).opacity(0.8)
                .cornerRadius(10)

        }
    }
}
