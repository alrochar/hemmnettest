//
//  UIColor+Extension.swift
//  PropertiesTest
//
//  Created by Alejandro López Rocha on 2022-03-27.
//

import Foundation

import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int, alpha: Int = 0xFF) {
        self.init(
            red: CGFloat(red) / 255.0,
            green: CGFloat(green) / 255.0,
            blue: CGFloat(blue) / 255.0,
            alpha: CGFloat(alpha) / 255.0
        )
    }

    convenience init(hex: Int, alpha: Int = 0xFF) {
        self.init(
            red: (hex >> 16) & 0xFF,
            green: (hex >> 8) & 0xFF,
            blue: hex & 0xFF,
            alpha: alpha & 0xFF
        )
    }
}

extension UIColor {
    static let mainGreen = UIColor(hex: 0x77DD77)
}

