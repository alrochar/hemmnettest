//
//  Font+Modifier.swift
//  PropertiesTest
//
//  Created by Alejandro López Rocha on 2022-03-27.
//

import SwiftUI

struct BodyFont: ViewModifier {
    func body(content: Content) -> some View {
        content
            .foregroundColor(.black)
    }
}

extension View {
    func bodyStyle() -> some View {
        modifier(BodyFont())
    }
}
