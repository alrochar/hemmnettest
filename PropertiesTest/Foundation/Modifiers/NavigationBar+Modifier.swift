//
//  NavigationBar+Modifier.swift
//  PropertiesTest
//
//  Created by Alejandro López Rocha on 2022-03-27.
//

import SwiftUI
struct NavigationBarBackgroundModifier: ViewModifier {

    var textColor: UIColor
    var backgroundColor: UIColor

    init(backgroundColor: UIColor, textColor: UIColor) {
        self.backgroundColor = backgroundColor
        self.textColor = textColor
        let navigationBarAppearence = UINavigationBarAppearance()
        navigationBarAppearence.configureWithTransparentBackground()
        navigationBarAppearence.backgroundColor = .clear
        navigationBarAppearence.titleTextAttributes = [.foregroundColor: textColor]
        navigationBarAppearence.largeTitleTextAttributes = [.foregroundColor: textColor]

        UINavigationBar.appearance().standardAppearance = navigationBarAppearence
        UINavigationBar.appearance().compactAppearance = navigationBarAppearence
        UINavigationBar.appearance().scrollEdgeAppearance = navigationBarAppearence
        UINavigationBar.appearance().tintColor = textColor
    }

  func body(content: Content) -> some View {
    ZStack{
       content
        VStack {
          GeometryReader { geometry in
             Color(self.backgroundColor)
                .frame(height: geometry.safeAreaInsets.top)
                .edgesIgnoringSafeArea(.top)
              Spacer()
          }
        }
     }
  }
}

extension View {
    func navigationBarColor(_ backgroundColor: UIColor, textColor: UIColor) -> some View {
        self.modifier(NavigationBarBackgroundModifier(backgroundColor: backgroundColor, textColor: textColor))
    }
}
