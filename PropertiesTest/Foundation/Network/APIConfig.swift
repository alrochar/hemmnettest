//
//  APIConfig.swift
//  PropertiesTest
//
//  Created by Alejandro López Rocha on 2022-03-20.
//

import Foundation

public enum APIError: Error {
    case genericError
    case urlNotFound
}

public protocol APIRequest {
    var baseURLPath: String { get }
    var path: String { get }
    var completeURL: URL? { get }
}

public enum APIConstants {
    static let baseURL = "https://pastebin.com/raw/"
}
