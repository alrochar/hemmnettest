//
//  PropertiesTestApp.swift
//  PropertiesTest
//
//  Created by Alejandro López Rocha on 2022-03-20.
//

import SwiftUI

@main
struct PropertiesTestApp: App {
    var body: some Scene {
        WindowGroup {
            PropertyListView()
        }
    }
}
